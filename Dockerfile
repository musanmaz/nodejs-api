FROM node:12

MAINTAINER Mehmet Sirin Usanmaz <mehmetusanmaz@hotmail.com>

WORKDIR /usr/src/app
COPY package*.json ./
RUN npm install
COPY src/*.js .
EXPOSE 11130

CMD [ "node", "server.js" ]