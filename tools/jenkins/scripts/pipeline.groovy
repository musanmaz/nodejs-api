import jenkins.*
import jenkins.model.*
import hudson.*
import hudson.model.*
import hudson.util.PersistedList
import jenkins.model.Jenkins
import jenkins.branch.*
import jenkins.plugins.git.*
import org.jenkinsci.plugins.workflow.multibranch.*
import com.cloudbees.hudson.plugins.folder.*

String gitRepo = "https://gitlab.com/musanmaz/nodejs-api.git"
String credentialsId = ""
String jobName = "nodejs_api"
String jobDescription = "Node JS API Pipeline"
String jobScript = "Jenkinsfile"
String periodicFolderTrigger = "15"

Jenkins jenkins = Jenkins.instance
WorkflowMultiBranchProject mbp = jenkins.createProject(WorkflowMultiBranchProject.class, jobName)
mbp.description = jobDescription
mbp.getProjectFactory().setScriptPath(jobScript)
mbp.addTrigger(new com.cloudbees.hudson.plugins.folder.computed.PeriodicFolderTrigger(periodicFolderTrigger));

String id = "unneeded-non-null-ID"
String remote = gitRepo
String includes = "master"
String excludes = ""
boolean ignoreOnPushNotifications = false
GitSCMSource gitSCMSource = new GitSCMSource(id, remote, credentialsId, includes, excludes, ignoreOnPushNotifications)
BranchSource branchSource = new BranchSource(gitSCMSource)

PersistedList sources = mbp.getSourcesList()
sources.clear()
sources.add(branchSource)
jenkins.getItem(jobName).scheduleBuild()

jenkins.save()