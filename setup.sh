#!/bin/bash
echo "This installation supported only MacOS"
function installBrew {
    echo "######################### Started Brew Install #########################"
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
}

function installVirtualBox {
    echo "######################### Started Virtualbox Install #########################"
    brew cask install virtualbox
}

function installVagrant {
    echo "######################### Started Vagrant Install #########################"
    brew cask install vagrant
}

function installDocker {
    echo "######################### Started Docker Install #########################"
    brew cask install docker
}

function installKubernetes {
    echo "######################### Started Kubernetes Install #########################"
    vagrant up
}

for i; do 
    if [ "$i" == "-h" ]; then
        printf "Help for Setup Script\nAvaliable parameters:\n\tbrew\t\t:\tInstall Brew for MacOS\n\tvirtaulbox\t:\tInstall VirtualBox for MacOS\n\tvagrant\t\t:\tInstall Vagrant for MacOS\n\tdocker\t\t:\tInstall Docker for MacOS\n\tkubernetes\t:\tInstall Kubernetes for MacOS\n"  
        exit 0
    elif [ $i == "brew" ]; then
        installBrew
    elif [ $i == "virtualbox" ]; then
        installVirtualBox
    elif [ $i == "vagrant" ]; then
        installVagrant
    elif [ $i == "docker" ]; then
        installDocker
    elif [$i == "kubernetes"]; then
        installKubernetes
    else
        echo "Invalid parameter name"
    fi
done

echo "######################### Started Kubernetes Install #########################"
vagrant up 

echo "######################### Started Jenkins Docker Build #########################"
source tools/jenkins/scripts/parameters.env
docker build -t musanmaz/jenkins:1.0.0 -f tools/jenkins/Dockerfile tools/jenkins
docker push musanmaz/jenkins:1.0.0
echo "######################### Started Jenkins Install #########################"
kubectl create -f tools/jenkins/jenkins.yml --namespace devops
kubectl create -f tools/jenkins/jenkins-service.yml --namespace devops