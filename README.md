# NodeJS Api

A simple NodeJS Api application built on Jenkins and deployed on k8s
  
### Tech

Dillinger uses a number of open source projects to work properly:

* [jenkins] - CI&CD Tool
* [docker] - a tool designed to make it easier to create, deploy, and run applications by using containers.
* [kubernetes] - the container orchestrator 
* [ansible] - a tool for config management, deployment and task automation.
* [vagrant] - a tool for building and managing virtual machine environments in a single workflow. 
* [nodeJS] - evented I/O for the backend

### Installation

Install the required applications..
This script is only supported on macOS
```sh
$ ./setup.sh -h 
This installation supported only MacOS
Help for Setup Script
Avaliable parameters:
	brew		:	Install Brew for MacOS
	virtaulbox	:	Install VirtualBox for MacOS
	vagrant		:	Install Vagrant for MacOS
	docker		:	Install Docker for MacOS
	kubernetes	:	Install Kubernetes for MacOS
$./setup.sh brew virtualbox vagraant docker kubernetes
```

The script will install the kubernetes environment after installing the packages you selected.After installing the Kubernetes environment, Jenkins container image will be created.

You can access the Jenkins : http://jenkins.devops.svc.cluster.local:8080
Jenkins creates an "admin / admin" user.

With the groovy scripts prepared for the Jenkins container image, the required kubernetes config, nodejs config and pipeline config are created automatically.

default-user.groovy : 
```groovy 
import jenkins.model.*
import hudson.security.*

def env = System.getenv()
def jenkins = Jenkins.getInstance()

if(!(jenkins.getSecurityRealm() instanceof HudsonPrivateSecurityRealm))
    jenkins.setSecurityRealm(new HudsonPrivateSecurityRealm(false))
if(!(jenkins.getAuthorizationStrategy() instanceof GlobalMatrixAuthorizationStrategy))
    jenkins.setAuthorizationStrategy(new GlobalMatrixAuthorizationStrategy())

def user = jenkins.getSecurityRealm().createAccount(env.JENKINS_USER, env.JENKINS_PASS)

user.save()
jenkins.getAuthorizationStrategy().add(Jenkins.ADMINISTER, env.JENKINS_USER)
jenkins.save()
```
kubernetes-config.groovy : 
```groovy 
import hudson.model.*
import jenkins.model.*
import org.csanchez.jenkins.plugins.kubernetes.*
import org.csanchez.jenkins.plugins.kubernetes.volumes.workspace.EmptyDirWorkspaceVolume
import org.csanchez.jenkins.plugins.kubernetes.volumes.HostPathVolume

import org.csanchez.jenkins.plugins.kubernetes.model.KeyValueEnvVar

def kc
try {
    println("===> Configuring k8s...")
    String jenkinsName = System.getenv("JENKINS_NAME")
    String namespace = System.getenv("NAMESPACE")
    String agentImage = System.getenv("JENKINS_AGENT_IMAGE")
    String agentNodeSelector = System.getenv("JENKINS_AGENT_NODE_SELECTOR")
    def agentRequestCpu = System.getenv("JENKINS_AGENT_REQUEST_CPU") ?: "0.2"
    def agentLimitCpu = System.getenv("JENKINS_AGENT_LIMIT_CPU") ?: "1"
    def agentRequestMemory = System.getenv("JENKINS_AGENT_REQUEST_MEMORY") ?: "512Mi"
    def agentLimitMemory = System.getenv("JENKINS_AGENT_LIMIT_MEMORY") ?: "512Mi"

    if (Jenkins.instance.clouds) {
        kc = Jenkins.instance.clouds.get(0)
        println "Cloud config found: ${Jenkins.instance.clouds}"
    } else {
        kc = new KubernetesCloud("kubernetes")
        Jenkins.instance.clouds.add(kc)
        println "Cloud config added: ${Jenkins.instance.clouds}"
    }

    kc.setServerUrl("https://kubernetes.default")
    kc.setJenkinsUrl("http://${jenkinsName}.${namespace}.svc.cluster.local:8080")
    kc.setJenkinsTunnel("${jenkinsName}.${namespace}.svc.cluster.local:50000")
    kc.setSkipTlsVerify(false)
    kc.setNamespace("${namespace}")
    kc.setRetentionTimeout(5)
    kc.setConnectTimeout(5)
    kc.setReadTimeout(15)
    kc.setMaxRequestsPerHostStr("32")
    kc.setContainerCapStr("10")

    if(kc.templates) {
        kc.templates.clear()
    } else {
        kc.templates = []
    }
    def podTemplate = new PodTemplate()
    podTemplate.setLabel("jenkins-agent")
    podTemplate.setName("${jenkinsName}-agent")
    if(agentNodeSelector) {
        podTemplate.setYaml("""
apiVersion: v1
kind: Pod
spec:
  restartPolicy: Never
  nodeSelector:
    cloud.google.com/gke-nodepool: ${namespace}
            """)
    }
    podTemplate.setCommand("")
    podTemplate.setInstanceCapStr('10')
    podTemplate.setIdleMinutesStr('30')
    podTemplate.setNodeUsageMode('NORMAL')
    podTemplate.setWorkspaceVolume(new EmptyDirWorkspaceVolume(false))

    def volumes = []
    volumes << new HostPathVolume("/usr/bin/docker", "/usr/bin/docker")
    volumes << new HostPathVolume("/var/run/docker.sock", "/var/run/docker.sock")
    podTemplate.setVolumes(volumes)

    def envVars = []
    envVars << new KeyValueEnvVar("NAMESPACE", namespace)
    podTemplate.setEnvVars(envVars)

    ContainerTemplate ct = new ContainerTemplate("jnlp", agentImage)
    ct.setAlwaysPullImage(false)
    ct.setPrivileged(false)
    ct.setTtyEnabled(false)
    ct.setWorkingDir("/home/jenkins")
    ct.setArgs('${computer.jnlpmac} ${computer.name}')
    ct.setResourceRequestCpu(agentRequestCpu)
    ct.setResourceLimitCpu(agentLimitCpu)
    ct.setResourceRequestMemory(agentRequestMemory)
    ct.setResourceLimitMemory(agentLimitMemory)
    ContainerLivenessProbe lp = new ContainerLivenessProbe("netstat -tan | grep ESTABLISHED", 10, 60, 3, 15, 1)
    ct.setLivenessProbe(lp)
    podTemplate.setContainers([ct])
    kc.templates << podTemplate
    kc = null
    println("===> Configuring k8s completed")
}
catch(Exception e) {
    println "===> Failed to configure kubernetes: " + e
    System.exit(1)
}
finally {
    kc = null
}
```

pipeline.groovy : 
```groovy 
import jenkins.*
import jenkins.model.*
import hudson.*
import hudson.model.*
import hudson.util.PersistedList
import jenkins.model.Jenkins
import jenkins.branch.*
import jenkins.plugins.git.*
import org.jenkinsci.plugins.workflow.multibranch.*
import com.cloudbees.hudson.plugins.folder.*

String gitRepo = "https://gitlab.com/musanmaz/nodejs-api.git"
String credentialsId = ""
String jobName = "nodejs_api"
String jobDescription = "Node JS API Pipeline"
String jobScript = "Jenkinsfile"
String periodicFolderTrigger = "15"

Jenkins jenkins = Jenkins.instance
WorkflowMultiBranchProject mbp = jenkins.createProject(WorkflowMultiBranchProject.class, jobName)
mbp.description = jobDescription
mbp.getProjectFactory().setScriptPath(jobScript)
mbp.addTrigger(new com.cloudbees.hudson.plugins.folder.computed.PeriodicFolderTrigger(periodicFolderTrigger));

String id = "unneeded-non-null-ID"
String remote = gitRepo
String includes = "master"
String excludes = ""
boolean ignoreOnPushNotifications = false
GitSCMSource gitSCMSource = new GitSCMSource(id, remote, credentialsId, includes, excludes, ignoreOnPushNotifications)
BranchSource branchSource = new BranchSource(gitSCMSource)

PersistedList sources = mbp.getSourcesList()
sources.clear()
sources.add(branchSource)
jenkins.getItem(jobName).scheduleBuild()

jenkins.save()
```

nodejs-installer.groovy : 
```groovy 
import jenkins.model.*
import hudson.model.*
import jenkins.plugins.nodejs.tools.*
import hudson.tools.*

def inst = Jenkins.getInstance()

def desc = inst.getDescriptor("jenkins.plugins.nodejs.tools.NodeJSInstallation")
def nodeJsVersion = System.getenv("NODEJS_VERSION")
def versions = [
  "nodejs": nodeJsVersion
]
def installations = [];

for (v in versions) {
  def installer = new NodeJSInstaller(v.value, "", 100)
  def installerProps = new InstallSourceProperty([installer])
  def installation = new NodeJSInstallation(v.key, "", [installerProps])
  installations.push(installation)
}

desc.setInstallations(installations.toArray(new NodeJSInstallation[0]))

desc.save()  
```